name := "Scala-Akka-Backend"

version := "0.1"

scalaVersion := "2.13.7"

val akkaHttpVersion = "10.2.6"
val akkaVersion = "2.6.18"
val scalaTestVersion = "3.2.9"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
  "org.scalatest" %% "scalatest" % scalaTestVersion,
  "com.pauldijou" %% "jwt-spray-json" % "5.0.0",
  "com.lightbend.akka" %% "akka-stream-alpakka-mongodb" % "3.0.4",
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion,
  "com.typesafe.akka" %% "akka-actor-testkit-typed" % akkaVersion % Test,
  "ch.megard" %% "akka-http-cors" % "1.1.2",
  "ch.qos.logback" % "logback-classic" % "1.2.9"

)

