package persistence.repository.impl

import akka.stream.Materializer
import akka.stream.alpakka.mongodb.scaladsl.{MongoSink, MongoSource}
import akka.stream.scaladsl.{Sink, Source}
import akka.{Done, NotUsed}
import com.mongodb.reactivestreams.client.{MongoClients, MongoCollection}
import constants.MongoConstants._
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.model.Filters
import persistence.repository.Repository.Repository

import scala.concurrent.{ExecutionContext, Future}
import scala.reflect.ClassTag


object MongoDatabase {
  def apply[T](collection: String,
               codecRegistry: CodecRegistry)
              (implicit ct: ClassTag[T],
               materializer: Materializer,
               executionContext: ExecutionContext)
  : MongoDatabase[T] = {
    val client = MongoClients.create(HOST)
    val db = client.getDatabase(DATABASE)
    val requestedCollection = db
      .getCollection(collection, ct.runtimeClass)
      .withCodecRegistry(codecRegistry).asInstanceOf[MongoCollection[T]]
    val source: Source[T, NotUsed] =
      MongoSource(requestedCollection.find(ct.runtimeClass)).asInstanceOf[Source[T, NotUsed]]
    val rows: Future[Seq[T]] = source.runWith(Sink.seq[T])
    new MongoDatabase[T](rows, requestedCollection)
  }
}

class MongoDatabase[T] private(rows: Future[Seq[T]],
                               requestedCollection: MongoCollection[T])
                              (implicit materializer: Materializer,
                               executionContext: ExecutionContext)
  extends Repository[T] {
  override def getByEmail(email: String): Future[Option[T]] = rows.map {
    list =>
      list.filter {
        _.asInstanceOf[ {def email: String}].email == email
      }.headOption
  }

  override def getAll: Future[Seq[T]] = rows

  override def getByUserId(userId: Long): Future[Option[T]] = rows.map {
    list =>
      list.filter {
        _.asInstanceOf[ {def userId: Long}].userId == userId
      }.headOption
  }

  override def getById(id: AnyVal): Future[Option[T]] = rows.map {
    list =>
      list.filter {
        _.asInstanceOf[ {def _id: AnyVal}]._id == id
      }.headOption
  }

  override def save(obj: T): Future[T] = {
    val source = Source.single(obj)
    source.runWith(MongoSink.insertOne(requestedCollection)).map(_ => obj)
  }

  override def delete(id: AnyVal): Future[Done] = {
    val source = Source.single(id).map(i => Filters.eq("_id", id))
    source.runWith(MongoSink.deleteOne(requestedCollection))
  }
}