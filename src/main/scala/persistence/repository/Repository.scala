package persistence.repository

import akka.Done

import scala.concurrent.Future

object Repository {
  trait Repository[T] {
    def getAll: Future[Seq[T]]
    def getById(id: AnyVal): Future[Option[T]]
    def save(item: T): Future[T]
    def delete(id: AnyVal): Future[Done]
    def getByUserId(userId:Long): Future[Option[T]]
    def getByEmail(email: String): Future[Option[T]]
  }
}