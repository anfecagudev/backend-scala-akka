package persistence.entity

import org.bson.types.ObjectId
import spray.json.{DefaultJsonProtocol}

object ProductItem extends DefaultJsonProtocol{
  import ObjectIdSerializer._
  case class ProductItem(_id:Option[ObjectId], imageUrl: String, typed: String, code: String, unit: String, price: Double, description: String)
  implicit val productConverter = jsonFormat7(ProductItem)
}

