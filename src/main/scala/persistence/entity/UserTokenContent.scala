package persistence.entity

import spray.json._

object UserTokenContent extends DefaultJsonProtocol{
  case class UserTokenContent(username: String,
                              email: String,
                              imgUrl: String)
  implicit val googleTokenResponseConverter = jsonFormat3(UserTokenContent)
}
