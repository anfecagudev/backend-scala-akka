package persistence.entity

case class User(_id: Long, name: String, email: String, imageUrl: String, emailVerified: Boolean, provider: String)

