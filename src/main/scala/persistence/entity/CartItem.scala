package persistence.entity

import persistence.entity.ProductItem._
import spray.json.DefaultJsonProtocol

object CartItem extends DefaultJsonProtocol{
  case class CartItem(quantity: Double, productItem: ProductItem)
  implicit val cartItemConverter = jsonFormat2(CartItem)
}
