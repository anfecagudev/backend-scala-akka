package persistence.entity
import org.bson.types.ObjectId
import persistence.entity.CartItem._
import spray.json._

object CartDTO extends DefaultJsonProtocol {
  import ObjectIdSerializer._
  case class CartDTO(_id: Option[ObjectId], userId: Long, products: List[CartItem])
  implicit val cartDTOSerializer = jsonFormat3(CartDTO)
}




