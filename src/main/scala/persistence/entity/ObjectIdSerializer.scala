package persistence.entity

import org.bson.types.ObjectId
import spray.json.{JsString, JsValue, JsonFormat, deserializationError}

object ObjectIdSerializer {
  implicit object ObjectIdJsonFormat extends JsonFormat[ObjectId] {
    def write(x: ObjectId) = {
      require(x ne null)
      JsString(x.toHexString)
    }
    def read(value: JsValue) = value match {
      case JsString(x) => if (ObjectId.isValid(x.toString)) new ObjectId(x.toString) else deserializationError("Expected String as JsString, but got " + x)
      case x => deserializationError("Expected String as JsString, but got " + x)
    }
  }
}
