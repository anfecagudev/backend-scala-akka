package persistence.entity;

public interface ClassProviderInterface {
    <T> Class<T> create();
}
