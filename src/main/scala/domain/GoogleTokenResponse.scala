package domain

import spray.json.DefaultJsonProtocol


object GoogleTokenResponse extends DefaultJsonProtocol{
  case class GoogleTokenResponse(access_token: String,
                                 expires_in: Long,
                                 token_type: String,
                                 scope:String,
                                 id_token: String)
  implicit val googleTokenResponseConverter = jsonFormat5(GoogleTokenResponse)
}
