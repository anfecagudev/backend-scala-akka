package domain

import spray.json.DefaultJsonProtocol

object IdTokenRep extends DefaultJsonProtocol {
  case class IdTokenRep(iss: String,
                        azp: String,
                        aud: String,
                        sub: String,
                        hd: String,
                        email: String,
                        email_verified: Boolean,
                        at_hash: String,
                        name: String,
                        picture: String,
                        given_name: String,
                        family_name: String,
                        locale: String,
                        iat: Long,
                        exp: Long
                       )

  implicit val IdTokenConverter = jsonFormat15(IdTokenRep)
}