package macros

import macros.MacroTools.ClassProvider
import persistence.entity.ClassProviderInterface
import scala.language.experimental.macros
import scala.annotation.compileTimeOnly

object Macros {
    @compileTimeOnly("Creates the Class file")
    def createTypeClass[T]: ClassProviderInterface = macro ClassProvider.createClass[T]
}

