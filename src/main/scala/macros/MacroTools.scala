package macros

import persistence.entity.ClassProviderInterface

import scala.reflect.macros.whitebox

object MacroTools {
  object ClassProvider {
    def createClass[T: c.WeakTypeTag](c: whitebox.Context): c.Expr[ClassProviderInterface] = {
      import c.universe._
      val mainType = weakTypeOf[T]
      c.Expr[ClassProviderInterface](
        q"""
         new ClassProviderInterface{
           @SuppressWarnings(Array("unchecked"))
           def create[C]: Class[T] = {
               classOf[$mainType]
           }
         }
       """
      )
    }
  }
}
