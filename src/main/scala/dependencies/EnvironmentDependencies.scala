package dependencies

import Actors.RedirectActor
import akka.actor.typed.{ActorRef, ActorSystem, DispatcherSelector}
import akka.japi.Pair
import akka.stream.ActorMaterializer
import constants.MongoConstants.{CART_COLLECTION, PRODUCTS_COLLECTION, USER_COLLECTION}
import controller.{CartController, GoogleAuthController, ProductsController}
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import persistence.entity.CartDTO.CartDTO
import persistence.entity.CartItem.CartItem
import persistence.entity.ProductItem.ProductItem
import persistence.entity.User
import persistence.repository.impl.MongoDatabase
import service.UserService
import utils.AuthenticationTools

case class SaveRedirectionAddress(pair: Pair[String, String]) extends Redirection

case class ReturnRedirectionAddress(status: String, actorRef: ActorRef[Option[String]]) extends Redirection

case class Response(address: Option[String])

trait Redirection

trait EnvironmentDependencies {
  /**
   * CONCURRENCY
   */
  implicit val system: ActorSystem[Redirection] = ActorSystem(RedirectActor(), "system")
  implicit val materializer = ActorMaterializer
  implicit val executionContext = system.dispatchers.lookup(DispatcherSelector.fromConfig("dispatcher"))

  /**
   * UTILITIES
   */
  implicit val authenticationTools = new AuthenticationTools

  /**
   * REPOSITORIES
   */
  implicit val userRepository = MongoDatabase[User](USER_COLLECTION, fromRegistries(fromProviders(classOf[User]), DEFAULT_CODEC_REGISTRY))
  implicit val productsRepository = MongoDatabase[ProductItem](PRODUCTS_COLLECTION, fromRegistries(fromProviders(classOf[ProductItem]), DEFAULT_CODEC_REGISTRY))
  implicit val cartRepository = MongoDatabase[CartDTO](CART_COLLECTION, fromRegistries(fromProviders
        (classOf[ProductItem],
          classOf[CartItem],
          classOf[CartDTO]), DEFAULT_CODEC_REGISTRY))

  /**
   * SERVICES
   */
  implicit val userService = new UserService
  //
  /**
   * CONTROLLERS */

  implicit val googleController = new GoogleAuthController
  implicit val productsController = new ProductsController
  implicit val cartController = new CartController
}