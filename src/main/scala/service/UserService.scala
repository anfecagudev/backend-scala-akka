package service

import constants.GeneralConstants._
import persistence.entity.CartDTO.CartDTO
import persistence.entity.User
import persistence.repository.Repository._
import utils.AuthenticationTools

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Random, Success}


class UserService(implicit authRepository: Repository[User],
                  aT: AuthenticationTools,
                  ec: ExecutionContext,
                  cartRepository: Repository[CartDTO]) {

  def getUserToken(code: String) = {
    aT.getTokenFromGoogle(code).flatMap { idToken =>
      authRepository.getByEmail(idToken.email)
        .flatMap { userOption =>
          if (userOption.get == None) {
            authRepository.save(User(Random.nextInt(2500), idToken.name, idToken.email, idToken.picture, idToken.email_verified, PROVIDER))
          } else {
            Future(userOption.get)
          }
        }
    }
      .map { user =>
        Option(aT.createToken(user))
      }
  }

  def getCart(token: String) = {
    val email = aT.getMailFromToken(token)
    authRepository.getByEmail(email).flatMap { userOption =>
      val user = userOption.get
      cartRepository.getByUserId(user._id)
    }
  }
}
