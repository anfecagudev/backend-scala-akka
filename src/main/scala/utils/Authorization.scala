package utils
import akka.actor.typed.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{HttpMethods, HttpRequest}
import akka.stream.Materializer
import akka.stream.scaladsl.{Sink, Source}
import akka.util.ByteString
import constants.AuthConstants._
import domain.GoogleTokenResponse.GoogleTokenResponse
import pdi.jwt.{JwtClaim, JwtSprayJson}
import persistence.entity.User
import persistence.entity.UserTokenContent.UserTokenContent
import spray.json._
import dependencies.Redirection

import java.util.concurrent.TimeUnit
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class AuthenticationTools(implicit system: ActorSystem[Redirection], materializer: Materializer, executionContext: ExecutionContext) {

  def createToken(user: User): String = {
    val claims = JwtClaim(
      expiration = Some(System.currentTimeMillis() / 1000 + TimeUnit.HOURS.toSeconds(EXPIRATION_IN_HOURS)),
      issuedAt = Some(System.currentTimeMillis() / 1000),
      issuer = Some("Akka-Backend"),
      content = UserTokenContent(user.name, user.email, user.imageUrl).toJson.prettyPrint
    )
    JwtSprayJson.encode(claims, SECRET_KEY, ALGORITHM)
  }

  def isTokenValid(token: String) = JwtSprayJson.isValid(token, SECRET_KEY, Seq(ALGORITHM))
  def isTokenExpired(token : String) = JwtSprayJson.decode(token, SECRET_KEY, Seq(ALGORITHM)) match {
    case Success(claims) => claims.expiration.getOrElse(0L) < System.currentTimeMillis() / 1000
    case Failure(exception) => true
  }
  def getMailFromToken(token:String) = JwtSprayJson.decode(token, SECRET_KEY, Seq(ALGORITHM)) match {
    case Success(claims) => claims.content.parseJson.convertTo[UserTokenContent].email
    case Failure(exception) => throw new RuntimeException("Couldn't get Email from token")
  }

  def streamMigrationFile(source: Source[ByteString, _]): Future[String] = {
    val sink = Sink.fold[String, ByteString]("") {
      case (acc, str) =>
        acc + str.decodeString("US-ASCII")
    }
    source.runWith(sink)
  }

  def getTokenFromGoogle(code: String) = {
    import utils.Util._
    Http().singleRequest(
      HttpRequest(
        method = HttpMethods.POST,
        uri = s"https://oauth2.googleapis.com/token?code=${
          code
        }&client_id=${
          CLIENT_ID
        }&client_secret=${
          CLIENT_SECRET
        }&redirect_uri=${
          REDIRECT_URI
        }&grant_type=authorization_code"
      ).withHeaders(RawHeader("Content-Type", "x-www-form-urlencoded"))
    ).flatMap(responseEntity => streamMigrationFile(responseEntity.entity.dataBytes))
      .map(_.parseJson.convertTo[GoogleTokenResponse].id_token.splitJWT.base64Decode.toToken)
  }
}