package utils

import spray.json._

import java.util.Base64

object Util {
  implicit class Util(str: String) {
    import domain.IdTokenRep._
    def splitJWT: String = str.split("\\.").toList(1)
    def base64Decode: String = new String(Base64.getDecoder.decode(str))
    def toToken: IdTokenRep = str.parseJson.convertTo[IdTokenRep]
  }
}




