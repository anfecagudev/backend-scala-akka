package constants

import pdi.jwt.JwtAlgorithm

case object AuthConstants {
  val CLIENT_ID = "474753632256-iu68roqaumuof54gccst7d2pe1mdf9vd.apps.googleusercontent.com"
  val CLIENT_SECRET = "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"
  val SCOPE = "https://www.googleapis.com/auth/drive.metadata.readonly"
  val REDIRECT_URI = "http://localhost:8081/oauth2/callback/google"
  val EXPIRATION_IN_HOURS = 1
  val ALGORITHM = JwtAlgorithm.HS256
  val SECRET_KEY = "5asd448we5s334asdf55qw546dfhgh56a5djfhsmkg5sdf456"
}
