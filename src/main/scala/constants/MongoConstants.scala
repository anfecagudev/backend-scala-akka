package constants

case object MongoConstants {
  val PORT = 27017
  val HOST = "mongodb://localhost:27017"
  val DATABASE = "products"
  val USER_COLLECTION = "user"
  val PRODUCTS_COLLECTION = "productItemDTTO"
  val CART_COLLECTION = "cartDTO"
}
