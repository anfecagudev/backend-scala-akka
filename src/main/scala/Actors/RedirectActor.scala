package Actors

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import akka.japi.Pair
import dependencies.{Redirection, ReturnRedirectionAddress, SaveRedirectionAddress}

object RedirectActor {
  def apply(): Behavior[Redirection] =
    mapper(Map())

  private def mapper(redirections: Map[String, String]): Behavior[Redirection] =
    Behaviors.receive { (_, message) =>
      message match {
        case SaveRedirectionAddress(Pair(status, redirectAddress)) =>
          mapper(redirections + (status -> redirectAddress))
        case ReturnRedirectionAddress(status, ref) =>
          ref ! redirections.get(status)
          mapper(redirections - status)
      }
    }
}

