package controller

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.server.Directives._
import ch.megard.akka.http.cors.scaladsl.CorsDirectives.{cors, corsRejectionHandler}
import persistence.entity.ProductItem._
import persistence.repository.Repository.Repository
import spray.json._

import scala.concurrent.ExecutionContext

class ProductsController(implicit repository: Repository[ProductItem], executionContext: ExecutionContext) extends SprayJsonSupport {
  val productsRoute = handleRejections(corsRejectionHandler) {
    cors() {
      path("users" / "datalist") {
        get {
          val entityFuture = repository.getAll.map { list =>
            HttpEntity(
              ContentTypes.`application/json`,
              list.toJson.prettyPrint
            )
          }
          complete(entityFuture)
        }
      }
    }
  }
}
