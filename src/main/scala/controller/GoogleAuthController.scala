package controller


import akka.actor.typed.scaladsl.AskPattern.{Askable, schedulerFromActorSystem}
import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.japi.Pair
import akka.stream.Materializer
import akka.util.Timeout
import ch.megard.akka.http.cors.scaladsl.CorsDirectives._
import constants.AuthConstants._
import dependencies._
import service.UserService

import java.util.UUID.randomUUID
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Success}

class GoogleAuthController(implicit actorSystem: ActorSystem[Redirection],
                           userService: UserService,
                           executionContext: ExecutionContext,
                           materializer: Materializer)
  extends SprayJsonSupport {
  implicit val timeout = Timeout(3 seconds)

  def getRoute(code: String, state: String) = {
    val address = actorSystem ? { (ref: ActorRef[Option[String]]) => ReturnRedirectionAddress(state, ref) }
    val token = userService.getUserToken(code)
    address.zip(token)
  }

  val googleAuthRoute = handleRejections(corsRejectionHandler) {
    cors() {
      pathPrefix("oauth2") {
        path("authorize" / "google") {
          parameter(Symbol("redirect_uri")) { redUri =>
            get {
              val state = randomUUID().toString
              val address = actorSystem ! SaveRedirectionAddress(Pair(state, redUri))
              redirect(s"https://accounts.google.com/o/oauth2/v2/auth?scope=email%20profile&access_type=offline&response_type=code&state=${state}&redirect_uri=${REDIRECT_URI}&client_id=${CLIENT_ID}", StatusCodes.TemporaryRedirect)
            }
          }
        } ~
          path("callback" / "google") {
            parameters("state", "code") { (state, code) =>
              get {
                onComplete(getRoute(code, state)) {
                  case Success((Some(address), Some(token))) => redirect(s"${address}?token=${token}", StatusCodes.PermanentRedirect)
                  case Failure(ex) => complete(StatusCodes.BadRequest)
                }
              }
            }
          }
      }
    }
  }
}
