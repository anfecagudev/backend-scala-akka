package controller

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives.{complete, get, handleRejections, path, _}
import ch.megard.akka.http.cors.scaladsl.CorsDirectives.{cors, corsRejectionHandler}
import persistence.entity.CartDTO._
import service.UserService
import utils.AuthenticationTools

import scala.concurrent.ExecutionContext

class CartController(implicit executionContext: ExecutionContext,
                     aT: AuthenticationTools,
                     service: UserService)
  extends SprayJsonSupport {
  val cartRoute = handleRejections(corsRejectionHandler) {
    cors() {
      pathPrefix("cart") {
        path("cart") {
          get {
            optionalHeaderValueByName("Authorization") {
              case Some(token) =>
                val trimmedToken = token.substring(7)
                if (aT.isTokenValid(trimmedToken)) {
                  if (aT.isTokenExpired(trimmedToken)) {
                    complete(HttpResponse(status = StatusCodes.Unauthorized, entity = "Token Expired."))
                  } else {
                    val cartEntity = service.getCart(trimmedToken)
                      .map {
                      case Some(cart) => HttpEntity(ContentTypes.`application/json`, cart.toJson.prettyPrint)
                      case None => throw new RuntimeException("No cart Found")
                    }
                    complete(cartEntity)
                  }
                } else {
                  complete(HttpResponse(status = StatusCodes.Unauthorized, entity = "Invalid Token."))
                }
              case None =>
                println("No token provided")
                complete(StatusCodes.Unauthorized)
            }
          }
        }
      }
    }
  }
}
