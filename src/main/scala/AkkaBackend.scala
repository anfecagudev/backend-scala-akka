import akka.http.scaladsl.Http
import dependencies.EnvironmentDependencies
import akka.http.scaladsl.server.Directives._

object AkkaBackend extends App with EnvironmentDependencies {
  val authServlet = googleController.googleAuthRoute
  val productServlet = productsController.productsRoute
  val cartServlet = cartController.cartRoute
  val servlet =
    authServlet ~ productServlet ~ cartServlet
  Http().newServerAt("localhost", 8081).bind(servlet)
}
